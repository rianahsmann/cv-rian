import {Component} from '@angular/core';
import { TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent {

  displayedColumns: string[] = ['period', 'institute', 'description'];
  dataSource;

  constructor(public translateService: TranslateService,
  ) {
    this.dataSource = translateService.get('education.fields');

    translateService.onLangChange.subscribe(() =>
      this.dataSource = translateService.get('education.fields')
    );
  }

}
