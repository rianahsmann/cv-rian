import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent {
  displayedColumns: string[] = ['period', 'company', 'function', 'description'];
  dataSource;
  constructor(public translateService: TranslateService,
  ) {
    this.dataSource = translateService.get('experience.fields');

    translateService.onLangChange.subscribe((next) =>
      this.dataSource = translateService.get('experience.fields')
    );
  }
}
