import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-language-select',
  templateUrl: './language-select.component.html',
  styleUrls: ['./language-select.component.scss']
})
export class LanguageSelectComponent implements OnInit {

  selected = this.translate.currentLang;

  constructor(public translate: TranslateService,
              private cdr: ChangeDetectorRef) {
    this.translate.addLangs(['en', 'nl']);


    if (this.translate.langs.some((lang: string) => lang === this.translate.getBrowserLang())) {
      this.translate.setDefaultLang(this.translate.getBrowserLang());
    } else {
      this.translate.setDefaultLang('nl');

    }
  }

  ngOnInit(): void {


  }


  onLanguageChange(newLang: string) {

    console.log(newLang);
    this.translate.use(newLang)
    this.cdr.detectChanges();
  }
}
